# Valheim maintenance

A simple script to maintain your LinuxGSM valheim server

The script needs a few variables to work, set WORLD to an asterisk ( * ) to update all worlds

When run it will:
- Stop the server to initiate maintenance
- Create backups of the game world
- Create backups of the server files
- Update the game server
- Update LinuxGSM
- Cleanup older backup data
- Start the server

Run nightly in cron to keep everything automatically updated and automatically create daily backups.
