#!/bin/bash

# Set env vars here:

# Valheim game world directory
WORLDSDIR="/home/${USER}/.config/unity3d/IronGate/Valheim/worlds"

# World name
WORLD='myworld'

# LinuxGSM main dir
LGSMDIR='/opt/linuxgsm'

# vhserver script location
LGSMSCRIPT="${LGSMDIR}/vhserver"

# Dir for world and linuxgsm backups
BACKUPDIR='/opt/lgsm_backups'

# Valhein systemd service id, leave empty if there is no service defined
SVCNAME=""

# Set to True to compress world backup files with gzip
GZIP='True'

# Uncomment string below to enable debugging
#set -x

log_msg () {
    echo "${1}: ${2}"
    logger -p user.${1} -t ${0} "${2}"
}

if [[ ! -x ${LGSMSCRIPT} ]]; then
    log_msg "error" "vhserver [${LGSMSCRIPT}] not found or executable."
    exit 1
fi

log_msg "info" "Stopping valheim server..."

if [[ -n ${SVCNAME} ]]; then
    sudo systemctl stop ${SVCNAME}
else
    ${LGSMSCRIPT} stop
fi

log_msg "info" "Performing server and world backups..."

timestamp=$(date +%F_%s)
if [[ $(find ${WORLDSDIR}/${WORLD}.db) ]]; then
    mkdir -p ${BACKUPDIR}
    cp ${WORLDSDIR}/${WORLD}.db \
        "${BACKUPDIR}/${WORLD}.db_${timestamp}"
    cp ${WORLDSDIR}/${WORLD}.fwl \
        "${BACKUPDIR}/${WORLD}.fwl_${timestamp}"
fi

if [[ ${GZIP} == 'True' ]]; then
    log_msg "info" "Compressing worldfile backup..."
    gzip -v --best "${BACKUPDIR}/${WORLD}.db_${timestamp}"
fi

if [[ -f ${LGSMDIR}/lgsm/lock/backup.lock ]]; then
    log_msg "info" "Deleting backup lockfile..."
    sleep 15
    unlink ${LGSMDIR}/lgsm/lock/backup.lock
fi

${LGSMSCRIPT} backup

if [[ $(find ${BACKUPDIR}/ -type f -name "vhserver*.tar.gz" | wc -l) -ge 2 ]]; then
    log_msg "info" "Cleaning up old server backups..."
    find ${BACKUPDIR}/ -type f -name "vhserver*.tar.gz" -mtime +2 -delete
fi

if [[ $(find ${BACKUPDIR}/ -type f -name "${WORLD}.db*" | wc -l) -ge 2 ]]; then
    log_msg "info" "Cleaning up old world backups..."
    find ${BACKUPDIR}/ -type f -name "${WORLD}*" -mtime +14 -delete
fi

log_msg "info" "Updating lgsm & valheim server"

${LGSMSCRIPT} update-lgsm

${LGSMSCRIPT} update

log_msg "info" "Starting valheim server..."

if [[ -n ${SVCNAME} ]]; then
    sudo systemctl start valheim
else
    ${LGSMSCRIPT} start
fi

if [[ ${?} != '0' ]]; then
    log_msg "error" "Vhserver startup failed."
    exit 1
else
    log_msg "info" "Vhserver started successfully."
    exit 0
fi
